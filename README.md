# mc-textures

Repository to compare the current [Minecraft](https://minecraft.net/) vanilla
resource pack with the new textures by [Jappa](https://twitter.com/jasperboerstra).

Links to comparisons (may take a while to load):

* [Vanilla and Latest Betapack (2)](https://gitlab.com/Noahkiq/mc-textures/compare/0e364509f9d07f0482039d517fc1c2a793790573...8e16fd3cef2ab0dde4e0fedc149afb411985b5a1)
* [Vanilla and Betapack 1](https://gitlab.com/Noahkiq/mc-textures/compare/0e364509f9d07f0482039d517fc1c2a793790573...f46c88b40c590fad4205728a44bbe859dbbf3dea)
* [Betapack 1 and 2](https://gitlab.com/Noahkiq/mc-textures/compare/f46c88b40c590fad4205728a44bbe859dbbf3dea...8e16fd3cef2ab0dde4e0fedc149afb411985b5a1)

Resource pack download links:

* [Latest Betapack](https://minecraft.net/article/try-new-minecraft-java-textures)
* [Betapack 2](https://community-content-assets-cms.minecraft.net/upload/f81517ca516363c381528480bb4c5189-New%20Default%20Betapack%20V2.zip)
* [Betapack 1](https://community-content-assets.minecraft.net/upload/8ecbc1e9fc691af61e0f16995ef5b097-NewDefault.zip)
